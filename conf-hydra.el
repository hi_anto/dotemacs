;; hydra
;; This is a package for GNU Emacs that can be used to tie related
;; commands into a family of short bindings with a common prefix - a Hydra.
(use-package hydra)

;; text scale
(defhydra hydra-text-scale (:timeout 4)
  "Scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "quit/cancel" :exit t))

;; window management
(defhydra hydra-winma (:columns 5)
  "Window management actions"
  ("0" delete-window "Delete")
  ("C-0" split-window-right "Split right")
  ("M-0" split-window-below "Split below")
  ("C-M-0" delete-other-windows "Other windows")
  
  ("h" windmove-left "Left")
  ("j" windmove-down "Down")
  ("k" windmove-up "Up")
  ("l" windmove-right "Right")
  
  ("C-h" windmove-swap-states-left "Swap left")
  ("C-j" windmove-swap-states-down "Swap down")
  ("C-k" windmove-swap-states-up "Swap up")
  ("C-l" windmove-swap-states-right "Swap right")

  ("M-h" shrink-window-horizontally "- window horizontally")
  ("M-j" shrink-window "- window")
  ("M-k" enlarge-window "+ window")
  ("M-l" enlarge-window-horizontally "+ window horizontally")

  ("q" nil "quit/cancel" :exit t))

;; movements
(defhydra hydra-move (:columns 5)
  "Moving"
  ("j" next-line "Next line")
  ("k" previous-line "Previous line")

  ("C-j" forward-char "Forward char")
  ("C-k" backward-char "Backward char")

  ("M-j" forward-word "Forward word")
  ("M-k" backward-word "Backward word")
  
  ("a" move-beginning-of-line "Beginning of line")
  ("e" move-end-of-line "End of line")

  ("M-e" forward-sentence "Forward sentence")
  ("M-a" backward-sentence "Backward sentence")

  ("]" forward-paragraph "Forward paragraph")
  ("[" backward-paragraph "Backward paragraph")
  ("J" forward-paragraph "Forward paragraph")
  ("K" backward-paragraph "Backward paragraph")
  
  ("G" beginning-of-buffer "Beginning of buffer")
  ("g" end-of-buffer "End of buffer")

  ("C-SPC" set-mark-command "Set mark")

  ("q" nil "quit/cancel" :exit t))

;; buffers
(defhydra hydra-kill-buff ()
  "Kill Buffers"
  ("0" kill-this-buffer "This")
  ("k" kill-buffer "Buffer")
  ("C-k" kill-matching-buffers "Matching buffers")
  ("M-0" kill-buffer-and-window "Buffer and window")
  
  ("q" nil "quit/cancel" :exit t))

;; case convertion
(defhydra hydra-case-convert (:columns 3)
  "Case Convertion"
  ("l" downcase-word "Downcase")
  ("u" upcase-word "Upcase")
  ("c" capitalize-word "Capitalize")
  ("C-l" downcase-region "Downcase region")
  ("C-u" upcase-region "Upcase region")
  
  ("q" nil "quit/cancel" :exit t))

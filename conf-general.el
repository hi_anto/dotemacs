;; general.el (more convenient key definitions in emacs)
(use-package general
  :config
  ;; * Global Keybindings
  ;; `general-define-key' acts like `global-set-key' when :keymaps is not
  ;; specified (because ":keymaps 'global" is the default)
  ;; kbd is not necessary and arbitrary amount of key def pairs are allowed

  ;; modus-theme toggle
  (general-define-key
   "M-`" 'modus-themes-toggle)

  ;; window management
  (general-define-key
   "M-0" 'hydra-winma/body)

  ;; movements
  (general-define-key
   "C-;" 'hydra-move/body)
  
  ;; editing
  (general-define-key
   "C-l" 'kill-whole-line
   "C-S-j" 'move-text-down
   "C-S-k" 'move-text-up
   "C-S-<mouse-1>" 'mc/add-cursor-on-click
   "M-c" 'hydra-case-convert/body)

  ;; editing [org-mode]
  (general-define-key
   :keymaps 'org-mode-map
   "C-l" 'kill-whole-line
   "C-S-j" 'move-text-down
   "C-S-k" 'move-text-up
   "C-M-i" 'completion-at-point)

  ;; org-roam
  (general-define-key
   "C-c n l" 'org-roam-buffer-toggle
   "C-c n f" 'org-roam-node-find
   "C-c n i" 'org-roam-node-insert)

  ;; buffer
  (general-define-key
   "M-x" 'counsel-M-x
   "C-b" 'counsel-ibuffer
   "C-f" 'counsel-find-file
   "M-k" 'hydra-kill-buff/body)

  ;; * Prefix Keybindings
  ;; :prefix can be used to prevent redundant specification of prefix keys
  (general-define-key
   :prefix "C-c"
   "p" 'projectile-command-map
   "C-s" 'hydra-text-scale/body))

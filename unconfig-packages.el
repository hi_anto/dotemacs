;; swiper
(use-package swiper)

;; counsel
(use-package counsel)

;; all-the-icons for use with doom-modeline
(use-package all-the-icons)

;; move-text
;; MoveText allows you to move the current line using M-up / M-down
;; (or any other bindings you choose) if a region is marked,
;; it will move the region instead.
(use-package move-text)

;; lorem-ipsum
(use-package lorem-ipsum)

;; ghub & forge
(use-package ghub)
(use-package forge)

;; expand-region
(use-package expand-region)

;; password-store
(use-package password-store)
(use-package pass)
(use-package ivy-pass)

;; yasnippet
(use-package yasnippet)

;; lsp-ivy
(use-package lsp-ivy)

;; evil-nerd-commenter
(use-package evil-nerd-commenter)

;; gitignore-templates
(use-package gitignore-templates)

;; fully featured terminal for emacs
(use-package vterm)

;; sudo-edit
(use-package sudo-edit)

;; this fucntion is to set the visual column width and make the text centered, and later hooked up to
;; org-mode so whenever emacs run org-mode the function is called
(defun hori/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

;; visual-fill-column
;; visual-fill-column-mode is a small Emacs minor mode that mimics the effect of fill-column in visual-line-mode.
;; Instead of wrapping lines at the window edge, which is the standard behaviour of visual-line-mode,
;; it wraps lines at fill-column. If fill-column is too large for the window, the text is wrapped at the window edge.
;; Told in images, visual-fill-column turns the view on the left into the view on the right,
;; without changing the contents of the file:
(use-package visual-fill-column
  :hook (org-mode . hori/org-mode-visual-fill))
